package com.example.iut_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Button validateButton = findViewById(R.id.validateButton);
        final EditText cityEditText = findViewById(R.id.cityPicker);
        final TextView textError = findViewById(R.id.textError);
        validateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://www.prevision-meteo.ch/services/json/" + cityEditText.getText().toString();

                RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                Intent startMeteoActivityIntent = new Intent(MainActivity.this, MeteoDisplayActivity.class);
                                try {
                                    JSONObject jsonResponse = new JSONObject(response);

                                    Boolean hasErrors = jsonResponse.has("errors");
                                    if(hasErrors){
                                        JSONArray errors = jsonResponse.getJSONArray("errors");
                                        textError.setVisibility(View.VISIBLE);
                                        int errorCode = errors.getJSONObject(0).getInt("code");
                                        String text = "Erreur inconnue";
                                        switch (errorCode){
                                            case 10:
                                                text = "Veuillez saisir une ville";
                                                break;
                                            case 11:
                                                text = "La ville saisie n'est pas valide";
                                                break;
                                        }
                                        textError.setText("Erreur : " + text);
                                    }
                                    else{
                                        JSONObject current = jsonResponse.getJSONObject("current_condition");
                                        JSONObject day_0 = jsonResponse.getJSONObject("fcst_day_0");
                                        JSONObject day_1 = jsonResponse.getJSONObject("fcst_day_1");
                                        JSONObject day_2 = jsonResponse.getJSONObject("fcst_day_2");
                                        JSONObject day_3 = jsonResponse.getJSONObject("fcst_day_3");
                                        JSONObject day_4 = jsonResponse.getJSONObject("fcst_day_4");

                                        startMeteoActivityIntent.putExtra("ville", cityEditText.getText().toString());

                                        startMeteoActivityIntent.putExtra("day_0_temperature_min", day_0.getString("tmin"));
                                        startMeteoActivityIntent.putExtra("day_0_temperature_max", day_0.getString("tmax"));
                                        startMeteoActivityIntent.putExtra("day_0_conditions", day_0.getString("condition"));
                                        startMeteoActivityIntent.putExtra("day_0_temperature", current.getString("tmp"));

                                        startMeteoActivityIntent.putExtra("day_1_temperature_min", day_1.getString("tmin"));
                                        startMeteoActivityIntent.putExtra("day_1_temperature_max", day_1.getString("tmax"));
                                        startMeteoActivityIntent.putExtra("day_1_conditions", day_1.getString("condition"));

                                        startMeteoActivityIntent.putExtra("day_2_temperature_min", day_2.getString("tmin"));
                                        startMeteoActivityIntent.putExtra("day_2_temperature_max", day_2.getString("tmax"));
                                        startMeteoActivityIntent.putExtra("day_2_conditions", day_2.getString("condition"));

                                        startMeteoActivityIntent.putExtra("day_3_temperature_min", day_3.getString("tmin"));
                                        startMeteoActivityIntent.putExtra("day_3_temperature_max", day_3.getString("tmax"));
                                        startMeteoActivityIntent.putExtra("day_3_conditions", day_3.getString("condition"));

                                        startMeteoActivityIntent.putExtra("day_4_temperature_min", day_4.getString("tmin"));
                                        startMeteoActivityIntent.putExtra("day_4_temperature_max", day_4.getString("tmax"));
                                        startMeteoActivityIntent.putExtra("day_4_conditions", day_4.getString("condition"));

                                        startActivity(startMeteoActivityIntent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("not ok");
                    }
                });

                queue.add(stringRequest);


            }
        });
    }
}
