package com.example.iut_project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class MeteoDisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteo_display);

        Intent intent = getIntent();
        TextView cityDisplayer = findViewById(R.id.cityDisplayer);
        TextView temperatureDisplayer = findViewById(R.id.temperatureDisplayer);
        TextView maxTemperatureDisplayer = findViewById(R.id.maxTemperatureDisplayer);
        TextView minTemperatureDisplayer = findViewById(R.id.minTemperatureDisplayer);
        TextView condition = findViewById(R.id.conditionDisplayer);

        String ville = intent.getStringExtra("ville");
        String day_0_temperature_min = "min : " + intent.getStringExtra("day_0_temperature_min") + "°C";
        String day_0_temperature_max = "max : " + intent.getStringExtra("day_0_temperature_max") + "°C";
        String day_0_conditions = intent.getStringExtra("day_0_conditions");
        String day_0_temperature = intent.getStringExtra("day_0_temperature") + "°C";

        String day_1_temperature_min = "min : " + intent.getStringExtra("day_1_temperature_min") + "°C";
        String day_1_temperature_max = "max : " + intent.getStringExtra("day_1_temperature_max") + "°C";
        String day_1_conditions = intent.getStringExtra("day_1_conditions");

        TextView tv_day_1_temperature_min = findViewById(R.id.jour1min);
        TextView tv_day_1_temperature_max = findViewById(R.id.jour1max);
        TextView tv_day_1_conditions = findViewById(R.id.jour1conditions);
        tv_day_1_temperature_min.setText(day_1_temperature_min);
        tv_day_1_temperature_max.setText(day_1_temperature_max);
        tv_day_1_conditions.setText(day_1_conditions);

        String day_2_temperature_min = "min : " + intent.getStringExtra("day_2_temperature_min") + "°C";
        String day_2_temperature_max = "max : " + intent.getStringExtra("day_2_temperature_max") + "°C";
        String day_2_conditions = intent.getStringExtra("day_2_conditions");

        TextView tv_day_2_temperature_min = findViewById(R.id.jour2min);
        TextView tv_day_2_temperature_max = findViewById(R.id.jour2max);
        TextView tv_day_2_conditions = findViewById(R.id.jour2conditions);
        tv_day_2_temperature_min.setText(day_2_temperature_min);
        tv_day_2_temperature_max.setText(day_2_temperature_max);
        tv_day_2_conditions.setText(day_2_conditions);

        String day_3_temperature_min = "min : " + intent.getStringExtra("day_3_temperature_min") + "°C";
        String day_3_temperature_max = "max : " + intent.getStringExtra("day_3_temperature_max") + "°C";
        String day_3_conditions = intent.getStringExtra("day_3_conditions");

        TextView tv_day_3_temperature_min = findViewById(R.id.jour3min);
        TextView tv_day_3_temperature_max = findViewById(R.id.jour3max);
        TextView tv_day_3_conditions = findViewById(R.id.jour3conditions);
        tv_day_3_temperature_min.setText(day_3_temperature_min);
        tv_day_3_temperature_max.setText(day_3_temperature_max);
        tv_day_3_conditions.setText(day_3_conditions);

        String day_4_temperature_min = "min : " + intent.getStringExtra("day_4_temperature_min") + "°C";
        String day_4_temperature_max = "max : " + intent.getStringExtra("day_4_temperature_max") + "°C";
        String day_4_conditions = intent.getStringExtra("day_4_conditions");

        TextView tv_day_4_temperature_min = findViewById(R.id.jour4min);
        TextView tv_day_4_temperature_max = findViewById(R.id.jour4max);
        TextView tv_day_4_conditions = findViewById(R.id.jour4conditions);
        tv_day_4_temperature_min.setText(day_4_temperature_min);
        tv_day_4_temperature_max.setText(day_4_temperature_max);
        tv_day_4_conditions.setText(day_4_conditions);

        cityDisplayer.setText(ville);
        temperatureDisplayer.setText(day_0_temperature);
        maxTemperatureDisplayer.setText(day_0_temperature_max);
        minTemperatureDisplayer.setText(day_0_temperature_min);
        condition.setText(day_0_conditions);

    }
}
